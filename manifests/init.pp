# The nrpe class
#
# @summary installs and configures the Nagios nrpe service
#
# @see "https://github.com/ghoneycutt/puppet-module-nrpe" for the original upstream source.  This file has been modified.
#
# @example Basic usage
#   
#    class {'::nrpe':
#        nrpe_package_ensure => '3.2.1-8.el7',
#        nagios_plugins_package_ensure => 'installed',
#        service_enable => false,
#        service_ensure => 'stopped',
#        allow_sudo => true,
#        plugins => {},
#    }
#
# @param nrpe_package
#   Name of package(s) for NRPE
#
# @param nrpe_package_ensure
#   String to pass to ensure attribute for the NRPE package.
#
# @param nrpe_package_adminfile
#   Path to admin file for NRPE package.
# 
# @param nrpe_package_source
#   Source to NRPE package.
# 
# @param nrpe_package_provider
#   Name of the package provider for the NRPE package.
# 
# @param nagios_plugins_package
#   Name of package(s) for nagios-plugins.
# 
# @param nagios_plugins_package_ensure
#   String to pass to ensure attribute for the nagios plugins package.
# 
# @param nagios_plugins_package_adminfile
#   Path to admin file for nagios-plugins package.
# 
# @param nagios_plugins_package_source
#   Source to nagios-plugins package.
# 
# @param nrpe_config
#   Path to nrpe.cfg file.
# 
# @param nrpe_config_owner
#   Owner of nrpe.cfg file.
# 
# @param nrpe_config_group
#   Group of nrpe.cfg file.
# 
# @param nrpe_config_mode
#   Mode of nrpe.cfg file.
# 
# @param libexecdir
#   Directory in which nrpe plugins are stored.
# 
# @param libexecdir_owner
#   User who should own the libexecdir directory
# 
# @param log_facility
#   The syslog facility that should be used for logging purposes.
# 
# @param pid_file
#   File in which the NRPE daemon should write it's process ID number. To disable the use of the pid_file parameter, specify the value as 'absent'.
# 
# @param server_port
#   Integer port number for nrpe between 0 and 65535, inclusive.
# 
# @param server_address_enable
#   Boolean to include server_address in nrpe.cfg.
# 
# @param server_address
#   Address that nrpe should bind to in case there are more than one interface and you do not want nrpe to bind on all interfaces.
# 
# @param nrpe_user
#   This determines the effective user that the NRPE daemon should run as.
# 
# @param nrpe_group
#   This determines the effective group that the NRPE daemon should run as.
# 
# @param allowed_hosts
#   Array of IP address or hostnames that are allowed to talk to the NRPE daemon.
# 
# @param dont_blame_nrpe
#   Boolean to determine whether or not the NRPE daemon will allow clients to specify arguments to commands that are executed. false=do not allow arguments, true=allow command arguments.
# 
# @param allow_bash_command_substitution
#   Boolean to determine whether or not the NRPE daemon will allow clients to specify arguments that contain bash command substitutions. false=do not allow, true=allow. Allowing is a **HIGH SECURITY RISK**.
# 
# @param command_prefix_enable
#   Boolean to include command_prefix in nrpe.cfg.
# 
# @param command_prefix
#   Prefix all commands with a user-defined string. Must be a fully qualified path.
# 
# @param debug
#   If debugging messages are logged to the syslog facility. Values: false=debugging off, true=debugging on
# 
# @param command_timeout
#   Maximum number of seconds that the NRPE daemon will allow plugins to finish executing before killing them off.
# 
# @param connection_timeout
#   Maximum number of seconds that the NRPE daemon will wait for a connection to be established before exiting.
# 
# @param allow_weak_random_seed
#   Allows SSL even if your system does not have a /dev/random or /dev/urandom. Values: false=only seed from /dev/[u]random, true=also seed from weak randomness
# 
# @param include_dir
#   Include definitions from config files (with a .cfg extension) recursively from specified directory.
# 
# @param service_ensure
#   Value of ensure parameter for nrpe service. Valid values are 'running' and 'stopped'.
# 
# @param service_name
#   Value of name parameter for nrpe service.
# 
# @param service_enable
#   Boolean value of enable parameter for nrpe service.  Set this to `false` if you will have nrpe started by xinetd.
# 
# @param sudo_command
#   Full path to command to execute when running plugins under sudo
# 
# @param allow_sudo
#   Boolean to manage a sudoers.d file to enable the use of sudo to invoke plugins.  If set to `true`, then the file /etc/sudoers.d/nagios is created giving the `$nrpe::user` user permission to run sudo for commands in `$nrpe::libexecdir`  without a password or tty.
# 
# @param hiera_merge_plugins
#   Boolean to control merges of all found instances of nrpe::plugins in Hiera. This is useful for specifying file resources at different levels of the hierarchy and having them all included in the catalog.
#
# This will default to 'true' in future versions.
# 
# @param purge_plugins
#   Boolean to purge the nrpe.d directory of entries not managed by Puppet.
# 
# @param plugins
#   Typically this attribute would be left undefined in the class instantiation and plugins added using the `nrpe::plugin_group` custom define as described below.
# 
# @param plugin_overrides
#   Hash of plugin settings to override defaults set in other levels of the data hierarchy.  This is merged with `nrpe::plugins` , as well as with any defined with `nrpe::plugin_group`
#
# Typically this attribute would be defined using hiera.
# 
# The layout of this Hash is identical to the layout of the plugins Hash for the `nrpe::plugin_group` custom define, with the exception that all attributes are optional.
#
class nrpe (
  String $nrpe_package,
  String $nrpe_package_ensure,
  $nrpe_package_adminfile = undef,
  $nrpe_package_source = undef,
  String $nagios_plugins_package,
  String $nagios_plugins_package_ensure,
  $nagios_plugins_package_adminfile = undef,
  $nagios_plugins_package_source = undef,
  String $nrpe_config,
  String $nrpe_config_owner,
  String $nrpe_config_group,
  String $nrpe_config_mode,
  String $libexecdir,
  String $libexecdir_owner,
  String $log_facility,
  String $pid_file,
  Integer[0, 65535] $server_port,
  Boolean $server_address_enable,
  String $server_address,
  String $nrpe_user,
  String $nrpe_group,
  Variant[String, Array[String]] $allowed_hosts,
  Boolean $dont_blame_nrpe,
  Boolean $allow_bash_command_substitution,
  Boolean $command_prefix_enable,
  String $command_prefix,
  Boolean $debug,
  Integer[1] $command_timeout,
  Integer[1] $connection_timeout,
  Boolean $allow_weak_random_seed,
  String $include_dir,
  Enum['running', 'stopped'] $service_ensure,
  String $service_name,
  Boolean $service_enable,
  Hash[String, Hash[String, Struct[{plugin => String,
                       Optional[args] => String,
		       Optional[use_sudo] => Boolean}]]] $plugins,
  Hash[String, Hash[String, Struct[{Optional[plugin] => String,
                       Optional[args] => String,
		       Optional[use_sudo] => Boolean}]]] $plugin_overrides,
  Boolean $purge_plugins,
  Boolean $hiera_merge_plugins,
  Optional[String] $nrpe_package_provider = undef,
  String $sudo_command,
  Boolean $allow_sudo,
) {

  # Convert types
  if $nrpe_package_provider {
    Package {
      provider => $nrpe_package_provider,
    }
  }

  package { $nrpe_package:
    ensure    => $nrpe_package_ensure,
    adminfile => $nrpe_package_adminfile,
    source    => $nrpe_package_source,
  }

  package { $nagios_plugins_package:
    ensure    => $nagios_plugins_package_ensure,
    adminfile => $nagios_plugins_package_adminfile,
    source    => $nagios_plugins_package_source,
    before    => [Service['nrpe_service'], File["${libexecdir}"]],
  }

  file {$libexecdir:
    ensure => 'directory',
    owner => $libexecdir_owner,
    group => $libexecdir_owner,
    mode => '0775',
  }

  file { 'nrpe_config':
    ensure  => file,
    content => epp('nrpe/nrpe.cfg.epp', {
	log_facility => $log_facility,
	pid_file => $pid_file,
	server_port => $server_port,
	server_address_enable => $server_address_enable,
	server_address => $server_address,
	nrpe_user => $nrpe_user,
	nrpe_group => $nrpe_group,
	allowed_hosts => $allowed_hosts,
	dont_blame_nrpe => $dont_blame_nrpe,
	allow_bash_command_substitution => $allow_bash_command_substitution,
	command_prefix_enable => $command_prefix_enable,
	command_prefix => $command_prefix,
	debug => $debug,
	command_timeout => $command_timeout,
	connection_timeout => $connection_timeout,
	allow_weak_random_seed => $allow_weak_random_seed,
	include_dir => $include_dir,
    }),
    path    => $nrpe_config,
    owner   => $nrpe_config_owner,
    group   => $nrpe_config_group,
    mode    => $nrpe_config_mode,
    require => Package[$nrpe_package],
  }

  file { 'nrpe_config_dot_d':
    ensure  => directory,
    path    => $include_dir,
    owner   => $nrpe_config_owner,
    group   => $nrpe_config_group,
    mode    => $nrpe_config_mode,
    purge   => $purge_plugins,
    recurse => true,
    require => Package[$nrpe_package],
    notify  => Service['nrpe_service'],
  }

  service { 'nrpe_service':
    ensure    => $service_ensure,
    name      => $service_name,
    enable    => $service_enable,
    subscribe => File['nrpe_config'],
  }

  if $allow_sudo {
      file { '/etc/sudoers.d/nagios':
	ensure  => 'file',
	owner   => 'root',
	group   => 'root',
	mode    => '0440',
	content => epp('nrpe/sudoers.epp', {
	    'user' => $nrpe_user,
	    'libexecdir' => $libexecdir,
	}),
      }

      if $facts['selinux'] or $facts['os']['selinux'] {
	  selboolean {'nagios_run_sudo':
	    persistent => true,
	    value => on,
	  }
      }
  } else {
      if $facts['selinux'] or $facts['os']['selinux'] {
	  selboolean {'nagios_run_sudo':
	    persistent => true,
	    value => off,
	  }
      }
  }

  $nrpe::plugins.each |String $group, Hash $plugin_hash| {
    file {"${include_dir}/${group}.cfg":
	ensure => file,
	owner => $nrpe_config_owner,
	group => $nrpe_config_group,
	mode => $nrpe_config_mode,
	content => epp('nrpe/nrpe_local.cfg.epp', {
	  'plugins' => deep_merge($plugin_hash, $plugin_overrides[$group]),
	  'libexecdir' => $libexecdir,
	  'sudo_command' => $sudo_command,
	}),
    }
  }
}
