# The nrpe::plugin_group defined type
#
# @summary Manage a file to be included from include_dir configuration in nrpe.cfg that does a remote check.
#
# @see "https://github.com/ghoneycutt/puppet-module-nrpe" for the original upstream source.  This file has been added.
#
# @example Basic usage
#
# @param ensure
#   Ensure the plugin group exists. Valid values are `present` and `absent`.
#
# @param plugins
#   A Hash of plugins to include in the group.  The key for each hash is the name to give the plugin.  The value is a structure with the three values described next.
#  plugin: The name of the executable in `nrpe::libexecdir` to run for the plugin.  All executables must live in `nrpe::libexecdir`.
#  args: Arguments to pass to the plugin
#  use_sudo: Boolean indicating if this plugin should be run using sudo.  If set to true, then the `nrpe::sudo_command` is prepended to the plugin executable.  For this to work properly, you will also need to set `nrpe::allow_sudo` to `true`, or create the proper /etc/sudoers entries manually.
#
define nrpe::plugin_group (
  Enum['present', 'absent'] $ensure = 'present',
  Hash[String, Struct[{plugin => String,
                       Optional[args] => String,
		       Optional[use_sudo] => Boolean}]] $plugins,
) {
  if $ensure == 'present' {
    $plugin_ensure = 'file'
  } else {
    $plugin_ensure = 'absent'
  }

  include ::nrpe

  file { "nrpe_plugin_${name}":
    ensure  => $plugin_ensure,
    path    => "${nrpe::include_dir}/${name}.cfg",
    content => epp('nrpe/nrpe_local.cfg.epp', {
	'plugins' => deep_merge($plugins, $nrpe::plugin_overrides[$name]),
	'libexecdir' => $nrpe::libexecdir,
	'sudo_command' => $nrpe::sudo_command,
    }),
    owner   => $nrpe::nrpe_config_owner,
    group   => $nrpe::nrpe_config_group,
    mode    => $nrpe::nrpe_config_mode,
    require => File['nrpe_config_dot_d'],
    notify  => Service['nrpe_service'],
  }
}
