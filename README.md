# nrpe
===

This module allows you to manage NRPE and its plugins. It does not name any of
the plugins, so you can use whatever you like by specifying a hash of plugins
and their associated parameters.

===

# Compatibility
---------------
This module was forked from "https://github.com/ghoneycutt/puppet-module-nrpe" to add the plugin_group define, add the plugin_overrides setting, and remove the calls to deprecated validation functions in puppet-stdlib.

It is tested on the following platforms:

* EL 6,7
* Solaris 11
* Ubuntu 12

# Class `nrpe`

## Parameters

nrpe_package
------------
Name of package(s) for NRPE.

- *Default*: based on OS platform.

nrpe_package_ensure
-------------------
String to pass to ensure attribute for the NRPE package.

- *Default*: 'present'

nrpe_package_adminfile
----------------------
Path to admin file for NRPE package.

- *Default*: based on OS platform. (used on Solaris)

nrpe_package_source
-------------------
Source to NRPE package.

- *Default*: based on OS platform. (used on Solaris)

nrpe_package_provider
-------------------
Name of the package provider for the NRPE package.

- *Default*: undef.

nagios_plugins_package
----------------------
Name of package(s) for nagios-plugins.

- *Default*: based on OS platform.

nagios_plugins_package_ensure
-----------------------------
String to pass to ensure attribute for the nagios plugins package.

- *Default*: 'present'

nagios_plugins_package_adminfile
--------------------------------
Path to admin file for nagios-plugins package.

- *Default*: based on OS platform. (used on Solaris)

nagios_plugins_package_source
-----------------------------
Source to nagios-plugins package.

- *Default*: based on OS platform. (used on Solaris)

nrpe_config
-----------
Path to nrpe.cfg file.

- *Default*: based on OS platform.

nrpe_config_owner
-----------------
Owner of nrpe.cfg file.

- *Default*: 'root'

nrpe_config_group
-----------------
Group of nrpe.cfg file.

- *Default*: 'root'

nrpe_config_mode
----------------
Mode of nrpe.cfg file.

- *Default*: '0644'

libexecdir
----------
Directory in which nrpe plugins are stored.

- *Default*: based on OS platform.

libexecdir_owner
----------
User who should own the libexecdir directory

- *Default*: based on OS platform.

log_facility
------------
The syslog facility that should be used for logging purposes.

- *Default*: 'daemon'

pid_file
--------
File in which the NRPE daemon should write it's process ID number. To disable the use of the pid_file parameter, specify the value as 'absent'.

- *Default*: based on OS platform.

server_port
-----------
Integer port number for nrpe between 0 and 65535, inclusive.

- *Default*: 5666

server_address_enable
---------------------
Boolean to include server_address in nrpe.cfg.

- *Default*: false

server_address
--------------
Address that nrpe should bind to in case there are more than one interface and you do not want nrpe to bind on all interfaces.

- *Default*: '127.0.0.1'

nrpe_user
---------
This determines the effective user that the NRPE daemon should run as.

- *Default*: based on OS platform

nrpe_group
---------
This determines the effective group that the NRPE daemon should run as.

- *Default*: based on OS platform

allowed_hosts
-------------
Array of IP address or hostnames that are allowed to talk to the NRPE daemon.

- *Default*: ['127.0.0.1']

dont_blame_nrpe
---------------
Boolean to determine whether or not the NRPE daemon will allow clients to specify arguments to commands that are executed. false=do not allow arguments, true=allow command arguments.

- *Default*: false

allow_bash_command_substitution
-------------------------------
Boolean to determine whether or not the NRPE daemon will allow clients to specify arguments that contain bash command substitutions. false=do not allow, true=allow. Allowing is a **HIGH SECURITY RISK**.

- *Default*: false

command_prefix_enable
---------------------
Boolean to include command_prefix in nrpe.cfg.

- *Default*: false

command_prefix
--------------
Prefix all commands with a user-defined string. Must be a fully qualified path.

- *Default*: '/usr/bin/sudo'

debug
-----
If debugging messages are logged to the syslog facility. Values: false=debugging off, true=debugging on

- *Default*: false

command_timeout
---------------
Maximum number of seconds that the NRPE daemon will allow plugins to finish executing before killing them off.

- *Default*: '60'

connection_timeout
------------------
Maximum number of seconds that the NRPE daemon will wait for a connection to be established before exiting.

- *Default*: '300'

allow_weak_random_seed
----------------------
Allows SSL even if your system does not have a /dev/random or /dev/urandom. Values: false=only seed from /dev/[u]random, true=also seed from weak randomness

- *Default*: false

include_dir
-----------
Include definitions from config files (with a .cfg extension) recursively from specified directory.

- *Default*: based on OS platform.

service_ensure
--------------
Value of ensure parameter for nrpe service. Valid values are 'running' and 'stopped'.

- *Default*: 'running'

service_name
--------------
Value of name parameter for nrpe service.

- *Default*: based on OS platform.

service_enable
--------------
Boolean value of enable parameter for nrpe service.  Set this to `false` if you will have nrpe started by xinetd.

- *Default*: true

sudo_command
--------------
Full path to command to execute when running plugins under sudo

- *Default*: based on OS platform.

allow_sudo
--------------
Boolean to manage a sudoers.d file to enable the use of sudo to invoke plugins.  If set to `true`, then the file /etc/sudoers.d/nagios is created giving the `$nrpe::user` user permission to run sudo for commands in `$nrpe::libexecdir`  without a password or tty.

- *Default*: false

hiera_merge_plugins
-------------------
Boolean to control merges of all found instances of nrpe::plugins in Hiera. This is useful for specifying file resources at different levels of the hierarchy and having them all included in the catalog.

This will default to 'true' in future versions.

- *Default*: false

purge_plugins
-------------
Boolean to purge the nrpe.d directory of entries not managed by Puppet.

- *Default*: false

plugins
-------
Typically this attribute would be left undefined in the class instantiation and plugins added using the `nrpe::plugin_group` custom define as described below.

- *Default*: undef

plugin_overrides
-------
Hash of plugin settings to override defaults set in other levels of the data hierarchy.  This is merged with `nrpe::plugins` , as well as with any defined with `nrpe::plugin_group`

Typically this attribute would be defined using hiera.

The layout of this Hash is identical to the layout of the plugins Hash for the `nrpe::plugin_group` custom define, with the exception that all attributes are optional.

- *Default*: undef

# Define `nrpe::plugin_group`

Creates a fragment in the nrpe.d directory with `$name.cfg`. Each matches the following layout, where `$args` are optional.

<pre>
command[$key]=${sudo_command} ${libexecdir}/${plugin} $args
</pre>

## Usage

<pre>
nrpe::plugin_group {'common':
    plugins => {
	'check_root_partition' => {
	    plugin => 'check_disk',
	    args => '-w 20% -c 10% -p /',
            use_sudo => true,
	},
	'check_load' => {
	    plugin => 'check_load',
	    args => '-w 10,8,8 -c 12,10,9',
	},
    }
}

nrpe::plugin_group {'cluster':
    ensure => 'present',
    plugins => {
	'check_hdfs' => {
	    plugin => 'check_hdfs',
	    args => '-w 10% -c 5% -p /hdfs',
	},
	'check_load' => {
	    plugin => 'check_load',
	    args => '-w 40,30,20 -c 20,10,5',
	},
    }
}
</pre>

You can optionally specify a hash of nrpe plugins in Hiera, so that plugins can be added at multiple levels of the data hierarchy.

<pre>
---
nrpe::plugins:
  common:
    check_root_partition:
      plugin: 'check_disk'
      args: '-w 20% -c 10% -p /'
      use_sudo: true
    check_load:
      plugin: 'check_load'
      args: '-w 10,8,8 -c 12,10,9'
  cluster:
    check_hdfs_partition:
      plugin: 'check_hdfs'
      args: '-w 10% -c 5% -p /hdfs'
    check_load:
      plugin: 'check_load'
      args: '-w 40,30,20 -c 20,10,5'
</pre>

Both the puppet and hiera examples above would produce two files in the nrpe.d directory called common.cfg and cluster.cfg, with the following contents:

<pre>
command[check_root_partition]=${nrpe::sudo_command} ${nrpe::libexecdir}/check_disk -w 20% -c 10% -p /
command[check_load]=${nrpe::libexecdir}/check_load -w 10,8,8 -c 12,10,9
</pre>

<pre>
command[check_hdfs_partition]=${nrpe::libexecdir}/check_hdfs -w 10% -c 5% -p /hdfs
command[check_load]=${nrpe::libexecdir}/check_load -w 40,30,20 -c 20,10,5
</pre>

## Parameters

ensure
------
Ensure the plugin group exists. Valid values are `present` and `absent`.

- *Default*: present

plugins
----
A Hash of plugins to include in the group.  The key for each hash is the name to give the plugin:

<pre>
command[$key]=${nrpe::sudo_command} ${nrpe::libexecdir}/${plugin} $args
</pre>

The value is a structure with the three values described next.

### plugins keys

plugin
------
The name of the executable in `nrpe::libexecdir` to run for the plugin.  All executables must live in `nrpe::libexecdir`.

This key is *Required*

args
----
Arguments to pass to the plugin.

This key is *Required*

use_sudo
--------
Boolean indicating if this plugin should be run using sudo.  If set to true, then the `nrpe::sudo_command` is prepended to the plugin executable.  For this to work properly, you will also need to set `nrpe::allow_sudo` to `true`, or create the proper /etc/sudoers entries manually.

This key is *Optional*

- *Default*: false
